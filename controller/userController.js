const jwt = require('jsonwebtoken')
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs')

const { User } = require('./../model')

const register = async (req, res) => {

    try{
        const { name, email, password } = req.body;

        if((name & email & password)){
            res.status(400).json({ message : "Required fields!"})
        }

        const checkUser = await User.findOne({ email })
    
        if(checkUser){
            res.status(409).json({ message : "User already exist"})
        }

        const encryptPassword = await bcrypt.hash(password, 10) 

        const user = await User.create({
            name,
            email,
            password: encryptPassword
        }) 

        const token = jwt.sign(
            { user_id : user._id, email},
            process.env.TOKEN_SECRET,
            { expiresIn : "2h"}
        )

        user.token = token;
        user.save((err, user) => {
            if(err) throw err;

            res.status(200).json({ user : user}) 
        })
    }catch(error){
        res.status(400).json({ message: error })
    }
}

const login = async (req, res) => {
    try{
        const { email, password } = req.body;

        if(!(email && password)){
            res.status(400).json({ message: "Email and Password is required"})
        }

        const user = await User.findOne({ email })

    
        if(!user){
            res.status(409).json({ message: "User not found"})
        }

        const checkPassword = await bcrypt.compare(password, user.password);

        if(!checkPassword){
            res.status(409).json({ message: "Wrong username/password"})
        }

        if(user && checkPassword){
            const token = jwt.sign({
                user_id : user.id, 
                email
            }, process.env.TOKEN_SECRET,
            { expiresIn : "2h"}
            )

            user.token = token;
            user.save((err, user) => {
                if(err) throw err;

                res.status(200).json({ user : user}) 
            })
        }

    }catch(error){
        res.status(400).json({ message: error })
    }
}

module.exports = {
    register,
    login
}