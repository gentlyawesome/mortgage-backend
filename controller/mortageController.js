const mortgage = require('mortgage-calculator');

const calcMortgage = async (req, res) => {
    const { initialDeposit, monthlyIncome, interest, terms,  monthlyExpense } = req.body;

    const result = mortgage.calculateMortgage({
      initialDeposit :  parseInt(initialDeposit), 
      monthlyIncome: parseInt(monthlyIncome), 
      interest: parseInt(interest), 
      terms:  parseInt(terms),  
      monthlyExpense :  parseInt(monthlyExpense)
    })

    res.status(200).json({ result })
}

module.exports = {
    calcMortgage
}