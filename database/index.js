const mongoose = require("mongoose");
require('dotenv').config();

const { MONGO_URI } = process.env;

exports.connect = async () => {
  try{
    await mongoose.connect(`${MONGO_URI}`, { useNewUrlParser: true })

    console.log("Successfully connected to database");
  }catch(error) {
    console.log({ error })
  }
};