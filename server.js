require('dotenv').config();
require('./database').connect();

const bodyParser = require('body-parser')
const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
const app = express()

app.use(morgan('dev'))

const corsOptions = {
  origin: (origin, callback) => {
    callback(null, true);
  },
  methods: ["GET", "POST", "PUT", "PATCH", "DELETE"],
  allowedHeaders: ["Access-Control-Allow-Origin", "Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization"],
  credentials: true
};

app.options('*', cors(corsOptions));
app.use(cors(corsOptions));

app.use(express.json({ limit: '100mb'}))
app.use(bodyParser.urlencoded({ extended : true }))
app.use(bodyParser.json())

app.use(cors({
  credentials: true,
  origin: process.env.BASE_SITE_URL
}));

const verifyToken = require('./middleware/auth');

const { userController, mortgageController } = require('./controller');

app.post('/api/register', userController.register)
app.post('/api/login', userController.login)
app.post('/api/calc-mortgage', mortgageController.calcMortgage)
app.get('/api/check-auth', verifyToken)

app.get('/api', (req, res) => {
    res.status(200).json({ ping: 'pong'})
})

app.listen(process.env.PORT, () => {
  console.log(`Example app listening at http://localhost:${process.env.PORT}`)
})